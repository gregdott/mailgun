<?php

require 'vendor/autoload.php';
use Mailgun\Mailgun;


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    $content = trim(file_get_contents("php://input"));
    $decoded_json = json_decode($content, true);
    if (!is_array($decoded_json)) {
        //throw new Exception('Received content contained invalid JSON!');
    }
    
    //-----------------------
    // Get data from json
    //-----------------------
    $email=$decoded_json['email'];
    $cc=$decoded_json['cc'];
    $api_key = $decoded_json['api_key'];
    $body = $decoded_json['body'];
    $subject = $decoded_json['subject'];
    $from = $decoded_json['from'];
    $domain = $decoded_json['domain'];
    
    
    // First, instantiate the SDK with your API credentials
    try {
        /*$mg = Mailgun::create($api_key, 'https://api.eu.mailgun.net'); // For EU servers
    
        // Now, compose and send your message.
        // $mg->messages()->send($domain, $params);
        $mg->messages()->send($domain, [
          'from'    => $from,
          'to'      => $email,
          'cc'      => $cc,
          'subject' => $subject,
          'text'    => $body
        ]);
        echo json_encode("success");*/
        # Instantiate the client.
        $mgClient = Mailgun::create($api_key, 'https://api.eu.mailgun.net');
        //$domain = "YOUR_DOMAIN_NAME";

        $recipients = array(
            $email
        );
        $params = array(
            'from' => $from,
            'subject' => $subject
        );

        $mime_string = $body;

        # Make the call to the client.
        $result = $mgClient->messages()->sendMime($domain, $recipients, $mime_string, $params);
    } catch (Exception $e) {
        echo json_encode($e);
        // ...   
    }
    
} else {
    //...
    echo json_encode("invalid request");
}